/*  Author: Arnaud Blanchard
   Example to demonstrate how to use blc_network.
 */

#include "blc_net_array.h"
#include <sys/time.h>

#define SIZE 1024

int main(int, char **){
    
    
    blc_net_array array_to_recv, array_to_send;
    blc_server server;
    
    fprintf(stderr, "\nExample of a program sending blc_array in udp on port '3333'\n'q' to quit\n");
    
    array_to_send.def_array('UIN8', 'NDEF', 2, 3, 3);
    array_to_send.allocate();
    
    array_to_send.chars[0]=1;
    
    array_to_send.send("localhost", "33333");
    
//    array_to_send.send_updates();
    
    while(getchar()!='q');
    
    server.stop_and_free_buffer();
    return EXIT_SUCCESS;
}