/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2016)
 
 Author: A. Blanchard
 
 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

//
//  Created by Arnaud Blanchard on 01/11/2016.
//

#include "blc_net_array.h"
#include "blc_realtime.h"
#include "blc_text.h"

#include <sys/select.h> //select
#include <unistd.h> //read


static blc_net_array **net_arrays=NULL;
static int net_arrays_nb=0;

static void *net_array_update_manager(void *user_data){
    blc_net_array *net_array = (blc_net_array*)user_data;
    fd_set fdset;
    ssize_t ret;
    struct timeval timeout={0, 100000}; //100ms
    
    //Wait for data to be ready
    FD_ZERO(&fdset);
    
    while(1){
        FD_SET(net_array->network.socket_fd, &fdset);
        SYSTEM_ERROR_CHECK(ret=select(net_array->network.socket_fd+1, &fdset, NULL, NULL, &timeout), -1, "Waiting server channel data"); //It would be nice to check errorfds
        if (ret==0) printf("Timeout for server\n");
        else{
            SYSTEM_ERROR_CHECK(ret=read(net_array->network.socket_fd, net_array->data,  net_array->size), -1, NULL);
            if (ret != (ssize_t)net_array->size) EXIT_ON_ERROR("Reading data: '%lu' bytes instead of '%lu'",  ret, net_array->size);
        }
    }
}

static void *refresh_server_cb(void *user_data){
    //  struct timespec time_left={0,0};
    struct timeval timer={0, 0};
    blc_net_array *net_array = (blc_net_array*)user_data;
    int current_duration;
    int64_t time_left;
    char buffer[1024];
    int32_t refresh_period=-1;
    ssize_t bytes_nb;
    
    net_array->sprint_properties(buffer, 1024);
    net_array->network.send_buffer(buffer, 1024);
    
    if (refresh_period==-2){
        while(1){
            SYSTEM_ERROR_CHECK(bytes_nb=read(net_array->network.socket_fd, net_array->data, net_array->size), -1, NULL);
            if (bytes_nb!=(ssize_t)net_array->size) EXIT_ON_ARRAY_ERROR(net_array, "Read only '%d' bytes instead of %d.", bytes_nb, net_array->size);
        }
    }
    else {
        
        
        while(1){
            net_array->network.send_buffer(net_array->data, net_array->size);
            
            if (refresh_period){
                if (refresh_period==-1){
                    read(net_array->network.socket_fd, &refresh_period, sizeof(refresh_period));
                }else if (refresh_period==-2)
                {
                    break;
                }else{
                    current_duration=blc_us_time_diff(&timer);
                    time_left = (net_array->refresh_period - current_duration);
                    if (time_left < 0) color_fprintf(BLC_YELLOW, stderr, "\rMissing %.3fms to send the channel.", -time_left/1000.f);
                    else SYSTEM_SUCCESS_CHECK(usleep(time_left), 0, "Program loop interrupted");
                    current_duration=blc_us_time_diff(&timer);
                    /*Warning usleep can be overtimed of 10ms !!! it is the same for usleep !!!*/
                }
            }
        }
    }
    return NULL;
}

/** It is called each time the port 33333 receive a message. If it is a known channel, a new port is opened specifically for the channel*/
static void net_array_request_cb(blc_server *server){
    struct sockaddr_storage address;
    socklen_t address_size;
    blc_net_array **tmp_array, *found_net_array=NULL;
    int32_t refresh_rate;
    
    refresh_rate=server->ints32[0];
    FOR_EACH(tmp_array, net_arrays, net_arrays_nb) if (strncmp((*tmp_array)->name, server->chars+sizeof(int32_t), server->size)==0){
        found_net_array=*tmp_array;
        break;
    }
    SYSTEM_ERROR_CHECK(getsockname(server->socket_fd, (struct sockaddr*)&address, &address_size), -1, NULL);
    
    if (found_net_array){
        found_net_array->network.init(server->chars, "0", BLC_UDP4 | BLC_SERVER);
        SYSTEM_ERROR_CHECK(getsockname(found_net_array->network.socket_fd, (struct sockaddr*)&address, &address_size), -1, NULL);
        found_net_array->network.remote_address=server->remote_address;
        found_net_array->network.remote_address_length=server->remote_address_length;
        BLC_PTHREAD_CHECK(pthread_create(&found_net_array->thread, NULL, refresh_server_cb, found_net_array), "Creating refresh cb");
    }
    else{
        snprintf(server->chars, server->size, "error: blc_net_channel does not exist.\n");
        server->send_back();
    }
}

void blc_net_array::init_server( char const *service_name, char const *port_name, void (*callback)(blc_net_array *net_array, void*), void* callback_arg, uint32_t type, uint32_t format, int dims_nb, int length0, ...){
    
    blc_net_array *this_pt=this;
    va_list arguments;
    
    va_start(arguments, length0);
    blc_array::vdef_array(type, format, dims_nb, length0, arguments);
    va_end(arguments);
    
    STRCPY(name, service_name);
    this->callback=callback;
    this->callback_arg=callback_arg;
    APPEND_ITEM(&net_arrays, &net_arrays_nb, &this_pt);
}


void blc_net_array::init_client(char const *service_name, char const *address, char const *port_name, int us_timeout){
    char  buffer[LINE_MAX];
    fd_set fdset;
    int ret;
    struct timeval timeout={0, us_timeout}; //100ms
    
    network.init(address, port_name, BLC_UDP4);
    //Send channel name request
    network.send_buffer(service_name, strlen(service_name));
    
    //Wait for data to be ready
    FD_ZERO(&fdset);
    FD_SET(network.socket_fd, &fdset);
    SYSTEM_ERROR_CHECK(ret=select(network.socket_fd+1, &fdset, NULL, NULL, &timeout), -1, "Waiting server channel definition"); //It would be nice to check errorfds
    if (ret==0) EXIT_ON_ERROR("Timeout of %.3fms. The server is probably down.", timeout.tv_sec*1000 + timeout.tv_usec/1000.f);
    
    network.remote_address_length=sizeof(network.remote_address);
    
    //Get the propoerties of the channel and the address (especilly the new port) of the sender
    SYSTEM_ERROR_CHECK(ret=recvfrom(network.socket_fd, buffer, LINE_MAX, NO_FLAG, (struct sockaddr*)&network.remote_address, &network.remote_address_length), -1, "Reading blc_channel properties");
    if (ret == LINE_MAX) EXIT_ON_ERROR("Buffer of '%d' is too small", ret);
    
    blc_array::init(buffer);
    allocate();
}

int blc_net_array::try_recv_update(int us_timeout){
    ssize_t ret;
    fd_set fdset;
    struct timeval timeout={0, us_timeout};
    
    FD_ZERO(&fdset);
    FD_SET(network.socket_fd, &fdset);
    
    network.send_buffer(&refresh_period, sizeof(refresh_period));
    SYSTEM_ERROR_CHECK(ret=select(network.socket_fd+1, &fdset, NULL, NULL, &timeout), -1, "Waiting server channel data"); //It would be nice to check errorfds
    if (ret==0) return 0; //Timeout
    SYSTEM_ERROR_CHECK(ret=read(network.socket_fd, data,  size), -1, NULL);
    
    if (ret!=(ssize_t)size)EXIT_ON_ERROR("Reading data: '%lu' bytes instead of '%lu'", ret, size);
    
    return 1;
}

void blc_net_array::recv_update(int us_timeout, int trials_nb){
    int i;
    
    FOR_INV(i, trials_nb) if (try_recv_update(us_timeout) == 1) return;
    EXIT_ON_ARRAY_ERROR(this, "Impossible to receive data in '%d' µs", us_timeout);
}

void blc_net_array::send_update(){    
    network.send_buffer(data, size);
}

void blc_net_array::start_refresh_updates(void (*callback)(blc_net_array*, void *), void *arg, int refresh_period, int us_timeout, int trials_nb){
    int i;
    
    this->callback=callback;
    this->callback_arg=arg;
    this->refresh_period = refresh_period;
    FOR_INV(i, trials_nb) if (try_recv_update(us_timeout) == 1) return;
    EXIT_ON_ARRAY_ERROR(this, "Impossible to receive data in '%d' µs. Stop after '%d' trials.", us_timeout, trials_nb);
    
    BLC_PTHREAD_CHECK(pthread_create(&thread, NULL, net_array_update_manager, this), NULL);
}

void blc_net_array_start_server(char const* port_name){
    
    blc_server server;
    server.start(port_name, BLC_UDP4, net_array_request_cb, NULL, LINE_MAX);
}





