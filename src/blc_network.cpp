//
//  blc_network.cpp
//  network
//
//  Created by Arnaud Blanchard on 30/05/2015.
//

#include "blc_network.h"
#include "blc_mem.h"
#include "blc_realtime.h"

#include <unistd.h>
#include <pthread.h>
#include <netdb.h>
#include <sys/types.h>

int blc_network::recv_buffer_timeout(void *buffer, size_t buffer_size, uint64_t us_timeout){
    fd_set fdset={{0}};
    ssize_t ret;
    struct timeval timeout;
    div_t result;
    
    FD_SET(socket_fd, &fdset);
    result=div(us_timeout, 1000000);
    timeout.tv_sec=result.quot;
    timeout.tv_usec=result.rem;
    SYSTEM_ERROR_CHECK(ret=select(socket_fd+1, &fdset, NULL, NULL, &timeout), -1, "Waiting server channel data"); //It would be nice to check errorfds
    if (ret==0) return 0;
    else{
        SYSTEM_ERROR_CHECK(ret=read(socket_fd, buffer, buffer_size), -1, NULL);
        if (ret != (ssize_t)buffer_size) EXIT_ON_ERROR("Reading data: '%lu' bytes instead of '%lu'",  ret, buffer_size);
    }
    return 1;
}

int blc_network::recv_timeout(uint64_t us_timeout){
    fd_set fdset = {{0}};
    ssize_t ret;
    struct timeval timeout;
    div_t result;
    
    FD_SET(socket_fd, &fdset);
    result=div(us_timeout, 1000000);
    timeout.tv_sec=result.quot;
    timeout.tv_usec=result.rem;
    SYSTEM_ERROR_CHECK(ret=select(socket_fd+1, &fdset, NULL, NULL, &timeout), -1, "Waiting server channel data"); //It would be nice to check errorfds
    if (ret==0) return 0;
    else{
        SYSTEM_ERROR_CHECK(ret=read(socket_fd, data, size), -1, NULL);
        if (ret != (ssize_t)size) EXIT_ON_ERROR("Reading data: '%lu' bytes instead of '%lu'",  ret, size);
    }
    return 1;
}

static void* server_manager(void *arg)
{
    blc_server *server=(blc_server*)arg;
    ssize_t received_data_size;
    
    if (server->data==NULL) EXIT_ON_ERROR("You have not allocated the receive buffer");
    server->remote_address_length=sizeof(struct sockaddr_storage);
    while(1) //pthread_cancel is used to stop it
    {
        //What if bufffer is too small ?
        SYSTEM_ERROR_CHECK(received_data_size = recvfrom(server->socket_fd, server->data, server->size,  0, (struct sockaddr*)&server->remote_address, &server->remote_address_length), -1, "Error receiving data.");
        server->received_data=received_data_size;
        server->callback(server);
    }
    return NULL;
}

static int blc_vset_iovec(struct iovec *iov, void *data, size_t size, va_list arguments){
    
    int len;
    for(len=0; data != NULL; len++)
    {
        iov[len].iov_base = data;
        iov[len].iov_len = size;
        data = va_arg(arguments, void*);
        size = va_arg(arguments, size_t);
    }
    return len;
    
}
int blc_set_iovec(struct iovec *iovec, void *data, size_t size, ...)
{
    int len;
    va_list arguments;
    
    va_start(arguments, size);
    len = blc_vset_iovec(iovec, data, size, arguments);
    va_end(arguments);
    return len;
}


void blc_set_msghdr(struct msghdr *msghdr, void *data, size_t size, ...)
{
    va_list arguments;
    
    va_start(arguments, size);
    msghdr->msg_iovlen=blc_vset_iovec(msghdr->msg_iov, data, size, arguments);
    va_end(arguments);
}



/* In the default case (client) we do not use connect to be able to read response from the server*/
void blc_network::init(char const *address_name, char const *port_name, int mode, size_t buffer_size){
    struct addrinfo hints, *results;
    int ret;
    struct sockaddr_storage address;
    socklen_t address_size;
    
    
    CLEAR(hints);
    hints.ai_flags = AI_NUMERICSERV; //We use only port number not service name ( faster )
    if (mode & BLC_SERVER) hints.ai_flags|= AI_PASSIVE; // if address is NULL it is ANY_ADDR otherwise. If not server it is loopback (~localhost)
    
    switch (mode & BLC_NET_PROTOCOLE){
            case BLC_UDP4:
            hints.ai_family = PF_INET;
            hints.ai_socktype = SOCK_DGRAM;
            break;
        default:
            EXIT_ON_ERROR("Unknown mode %d. The possibility is 'BLC_UDP4'.", mode);
            break;
    }
    ret=getaddrinfo(address_name, port_name, &hints, &results);
    if (ret !=0) EXIT_ON_ERROR(gai_strerror(ret)); //, 0, "setting address: %s", address_name));
    if(results->ai_next != NULL) EXIT_ON_ERROR("There is more than one possible address. It is not yet implemented.");
    SYSTEM_ERROR_CHECK(socket_fd = socket(results->ai_family, results->ai_socktype, results->ai_protocol), -1, NULL); //We suppose there is only one result in 'results'
    
    address_size = sizeof(address);
    SYSTEM_ERROR_CHECK(getsockname(socket_fd, (struct sockaddr*)&address, &address_size), -1, NULL);
    
    if (mode & BLC_SERVER)  SYSTEM_ERROR_CHECK(bind(socket_fd, results->ai_addr, results->ai_addrlen), -1, "Socket %d", socket_fd);
    else { //We do not use connect to allow the port to also read answers
        memcpy(&remote_address, (void*)results->ai_addr, results->ai_addrlen);
        remote_address_length=results->ai_addrlen;
    }
    freeaddrinfo(results);
    
    if (buffer_size) allocate(buffer_size);
}

void blc_network::get_remote_address(char *channel_name, socklen_t name_size, char *port_name, socklen_t port_name_size){
    int ret;
    
    ret=getnameinfo((const struct sockaddr *)&remote_address, remote_address_length, channel_name, name_size, port_name, port_name_size, NI_NUMERICSERV);
    if (ret !=0) EXIT_ON_ERROR(gai_strerror(ret)); //, 0, "setting address: %s", address_name));
}


void blc_network::send(){
    if (this->data==NULL) EXIT_ON_ERROR("You have not allocated buffer.");
    SYSTEM_ERROR_CHECK(sendto(socket_fd, this->data, this->size, 0, (struct sockaddr*)&remote_address, remote_address_length), -1, "socket_fd '%d', request first size '%lu'", socket_fd, remote_address_length);
}

void blc_network::send_buffer(void const* buffer, size_t size){
    ssize_t ret;
    
    SYSTEM_ERROR_CHECK(ret=sendto(socket_fd, buffer, size, NO_FLAG, (struct sockaddr*)&remote_address, remote_address_length), -1, "socket_fd '%d', request size '%u'", socket_fd, size);
    if ((size_t)ret!=size) EXIT_ON_ERROR("Only '%l' bytes sent instead of '%lu'", ret, size);
}

blc_network::~blc_network(){
    close(socket_fd);
}

void blc_server::start(char const *port_name, int protocol, void(*on_receive_data)(blc_server*), void *arg, size_t buffer_size){
    callback = on_receive_data;
    callback_arg = arg;
    
    init(NULL, port_name, protocol | BLC_SERVER, buffer_size);
    SYSTEM_SUCCESS_CHECK(pthread_create(&thread, NULL, server_manager, (void*)this), 0, "Starting server");
}

void blc_server::stop(){
    callback = NULL;
    callback_arg = NULL;
    
    BLC_PTHREAD_CHECK(pthread_cancel(thread), NULL);
    /*We wait that the thread has stopped before closing the socket*/
    BLC_PTHREAD_CHECK(pthread_join(thread, NULL), NULL);
    close(socket_fd);
}

void blc_server::stop_and_free_buffer(){
    stop();
}

void blc_server::send_back(){
    SYSTEM_ERROR_CHECK(sendto(socket_fd, data, size , NO_FLAG,  (struct sockaddr*)&remote_address, remote_address_length), -1, "socket %d", socket_fd);
}

void blc_server::send_back_buffer(void const *buffer, size_t buffer_size){
    //  printf("%s\n", (char*)buffer);
    SYSTEM_ERROR_CHECK(sendto(socket_fd, buffer, buffer_size , NO_FLAG,  (struct sockaddr*)&remote_address, remote_address_length), -1, "socket %d", socket_fd);
}

/* Network */
START_EXTERN_C
void blc_network_init(blc_network *network, char const *address_name, char const *port_name, int mode){
    network->init(address_name, port_name, mode);
}

void blc_server_start(blc_server *server, char const *port_name, int mode, void(*on_receive_data)(blc_server*), void *arg){
    server->start(port_name, mode, on_receive_data, arg);
}

void blc_server_allocate_mem_and_start(blc_server *server, char const *port_name, int mode, void(*on_receive_data)(blc_server*), void *arg, size_t buffer_size){
    server->start(port_name, mode, on_receive_data, arg, buffer_size);
}
END_EXTERN_C



