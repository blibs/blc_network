//
//  blc_network.h
//  network
//
//  Created by Arnaud Blanchard on 30/05/2015.
//
//

/**
 @defgroup blc_network network
 Few functions helping for pseudo realtime applications.
 @{*/
#ifndef BLC_NETWORK_H
#define BLC_NETWORK_H

#include <stdio.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include "blc_core.h"

/*By default it is a client*/
#define BLC_SERVER 1
#define BLC_UDP4 2
#define BLC_UDP4_SERVER (BLC_SERVER | BLC_UDP4)
#define BLC_NET_PROTOCOLE (BLC_UDP4) // FOr now it is use less but latter it will include all the protocoles

/* To be used with readv/writev, sendmsg/recvmsg. */
#define BLC_SET_IOVEC(iovec, variable)  blc_set_iovec(iovec, &(variable), sizeof(variable), NULL)
#define BLC_SET_IOVEC2(iovec, variable1, variable2)  blc_set_iovec(iovec, &(variable1), sizeof(variable1), &(variable2), sizeof(variable2), NULL)
#define BLC_SET_IOVEC3(iovec, variable1, variable2, variable3)  blc_set_iovec(iovec, &(variable1), sizeof(variable1),&(variable2), sizeof(variable2),&(variable3), sizeof(variable3), NULL)

typedef struct blc_server blc_server;

//typedef void (*type_blc_server_callback)(blc_server *server);

typedef struct blc_network
#ifdef __cplusplus
:blc_mem{
    /**Init a network connection in **mode** **BLC_UDP4** (udp IPv4) for now.
     **address** can be null (usually for a server). Acceptable value for **address**  is either a valid host name or a numeric host address string consisting of a dotted decimal IPv4
     **port_name** is a port number as a string of a decimal number.
     If buffer_size=0, you are responsible for allocating the memory in .data (use .allocate(size) for example).
     */

	~blc_network();
    void init(char const *address_name, char const *port_name, int mode, size_t buffer_size=0);
    void send();
    void send_buffer(void const* data, size_t size);
    
    // Return 1 if data as been read, 0 otherwise (timeout)
    int recv_timeout(uint64_t us_timeout);
    int recv_buffer_timeout(void *buffer, size_t size, uint64_t us_timeout);

    void get_remote_address(char *channel_name, socklen_t name_size, char *port_name, socklen_t port_name_size);

#else
    { blc_mem mem;
#endif

    struct sockaddr_storage remote_address;
    socklen_t remote_address_length;
    int socket_fd;

}blc_network;

typedef struct blc_server
#ifdef __cplusplus
:blc_network {
    /** start a *pthread* with a server listenning (*rcvmsg*) in the **port_name** (i.e. "3333") with **mode** BLC_UDP4 (only for now).
     Each time the server receive data on the associated port it will call the callback with the data received as parameter.
     **.mem**, the receiving buffer has to be allocated before with enough size to store all a received message.
     */
    void start(char const *port_name, int mode, void(*on_receive_data)(blc_server*) , void *arg, size_t buffer_size=0);
    
    /**stop (close(socket_fd)) the server*/
    void stop();
    
    /** stop the server and free the allocated receive buffer*/
    void stop_and_free_buffer();
    
    void send_back();

    void send_back_buffer(void const *buffer, size_t size);
#else
{ blc_network network;
#endif
    pthread_t thread;
    void(*callback)(blc_server*);
    void *callback_arg;
    size_t received_data;
}blc_server;
    
START_EXTERN_C
    
///Set a **iovev struct** used by sendmsg/recvmsg, with all the buffers (couples pointer/size) in the list. The list **has** to finish by **NULL**.
int blc_set_iovec(struct iovec *iov, void *data, size_t size, ...);
void blc_set_msghdr(struct msghdr *msghdr, void *data, size_t size, ...);
void blc_network_init(blc_network *network, char const *address_name, char const *port_name, int mode);
void blc_server_start(blc_server *server, char const *port_name, int mode, void(*on_receive_data)(blc_server*), void *);
void blc_server_allocate_mem_and_start(blc_server *server, char const *port_name, int mode, void(*on_receive_data)(blc_server*), void *, size_t size);
END_EXTERN_C

#endif
///@}
