//
//  blc_net_array.h
//
//  Created by Arnaud Blanchard on 01/11/2016.
//
//

/**
 @defgroup blc_net_array
 @{*/
#ifndef BLC_NET_ARRAY_H
#define BLC_NET_ARRAY_H

#include "blc_array.h"
#include "blc_network.h"

typedef struct blc_net_array
#ifdef __cplusplus
:blc_array{
    void init_server( char const *service_name, char const *port_name, void (*callback)(blc_net_array *net_array, void*), void* callback_arg, uint32_t type, uint32_t format, int dims_nb, int length0, ...);
    void init_client( char const *service_name, char const *address="localhost", char const *port_name="33333", int us_timeout=100000);
   
    int try_recv_update(int us_timeout=100000);
    void recv_update(int us_timeout = 100000, int trials_nb = 1);
    void send_update();

    void start_refresh_updates(void (*callback)(blc_net_array*, void *), void *arg, int refresh_period, int us_timeout, int trials_nb);
#else
    {
        blc_array array;
#endif
        blc_network network;
        char  name[NAME_MAX];
        int refresh_period;
        pthread_t thread;
        void (*callback)(blc_net_array*, void*);
        void *callback_arg;
        
}blc_net_array;
    
START_EXTERN_C
END_EXTERN_C

#endif
///@}