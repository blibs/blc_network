/*  Author: Arnaud Blanchard
   Example to demonstrate how to use blc_network.
 */

#include "blc_network.h"
#include <sys/time.h>
#include <netdb.h> //NI_MAXHOST, NI_MAXSERV


#define SIZE 1024

static void receive_cb(blc_server *server){
    struct timeval local_timer, *timer;
    
    gettimeofday(&local_timer, NULL);
    timer=(struct timeval*)server->data;
    fprintf(stderr, "Received %lu' bytes, text '%s' after %luµs\n", server->size,  server->chars+sizeof(*timer), (local_timer.tv_sec-timer->tv_sec)*1000000+(local_timer.tv_usec-timer->tv_usec));
    server->send_back_buffer((void*)"coucou", 6);
}

int main(int, char **){
    blc_server server;
    
    fprintf(stderr, "\nExample of a program reading udp messages on port '3333'\n'q' to quit\n");
    server.start("3333", BLC_UDP4, receive_cb, NULL, SIZE);
    
    while(getchar()!='q');
    
    server.stop_and_free_buffer();
    return EXIT_SUCCESS;
}