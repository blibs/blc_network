# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2016)
# Author: Arnaud Blanchard (November 2016)
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. 

find_package(blc_core REQUIRED)

find_path(BLC_NETWORK_INCLUDE_DIR blc_network.h PATH_SUFFIXES blc_network)
find_library(BLC_NETWORK_LIBRARY blc_network )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(blc_network DEFAULT_MSG  BLC_NETWORK_LIBRARY BLC_NETWORK_INCLUDE_DIR)

mark_as_advanced(BLC_NETWORK_INCLUDE_DIR BLC_NETWORK_LIBRARY )

set(BL_INCLUDE_DIRS ${BLC_NETWORK_INCLUDE_DIR} ${BL_INCLUDE_DIRS} )
set(BL_LIBRARIES ${BLC_NETWORK_LIBRARY} ${BL_LIBRARIES} )
