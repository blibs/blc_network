/*  Author: Arnaud Blanchard
   Example to demonstrate how to use blc_network.
 */

#include "blc_network.h"
#include <sys/time.h>

#define SIZE 1000000

static void receive_cb(char *buffer, size_t size, void *arg){
    struct timeval local_timer, *timer;
    
    gettimeofday(&local_timer, NULL);
    timer=(struct timeval*)buffer;
    fprintf(stderr, "Received %lu' bytes, text '%s' after %luµs\n", size,  buffer+sizeof(*timer), (local_timer.tv_sec-timer->tv_sec)*1000000+(local_timer.tv_usec-timer->tv_usec));
}

int main(int, char **){
    blc_server server;
    
    fprintf(stderr, "\nExample of a program reading udp messages on port '3333'\n'q' to quit\n");
    server.start("3333", BLC_UDP4, receive_cb, NULL, SIZE);
    
    while(getchar()!='q');
    
    server.stop_and_free_buffer();
    return EXIT_SUCCESS;
}