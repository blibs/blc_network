/*  Author: Arnaud Blanchard
 Example to demonstrate how to use blc_network.
 */


#include "blc_network.h"
#include <sys/time.h>

#define BUFFER_SIZE 1024

int main(int, char **){
    blc_network network;
    struct timeval timer;
    
    fprintf(stderr, "\nExample of a program sending udp messages on 'localhost', port '3333'\n'q' to quit\n");

    network.init("localhost", "3333", BLC_UDP4, BUFFER_SIZE);
    do{
        fprintf(stderr, "Text to send ?...\n");
        SYSTEM_ERROR_CHECK(fgets(network.chars+sizeof(timer), BUFFER_SIZE-sizeof(timer), stdin), NULL, NULL);
        gettimeofday((struct timeval*)network.data, NULL);
        network.send();
    } while(strncmp(network.chars+sizeof(timer), "q\n", 2)!=0);
    
    network.close_and_free_buffer();
    
    return EXIT_SUCCESS;
}