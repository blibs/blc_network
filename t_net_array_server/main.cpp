/*  Author: Arnaud Blanchard
 Example to demonstrate how to use blc_network.
 */


#include "blc_network.h"
#include <sys/time.h>
#include <unistd.h>

#define BUFFER_SIZE 1024

int main(int, char **){
    blc_network client;
    struct timeval timer;
    
    fprintf(stderr, "\nExample of a program sending udp messages on 'localhost', port '3333'\n'q' to quit\n");
    
    client.init("localhost", "3333", BLC_UDP4, BUFFER_SIZE);
    
    do{
        fprintf(stderr, "Text to send ?...\n");
        SYSTEM_ERROR_CHECK(fgets(client.chars+sizeof(timer), BUFFER_SIZE-sizeof(timer), stdin), NULL, NULL);
        gettimeofday((struct timeval*)client.data, NULL);
        client.send();
        SYSTEM_ERROR_CHECK(read(client.socket_fd, client.data, client.size), -1, NULL);
        fprintf(stderr, "Recv %s\n", client.chars);
        
    } while(strncmp(client.chars+sizeof(timer), "q\n", 2)!=0);
    
    client.close_and_free_buffer();
    
    return EXIT_SUCCESS;
}